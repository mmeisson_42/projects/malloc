# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/05/11 13:23:20 by mmeisson          #+#    #+#              #
#    Updated: 2017/05/17 16:16:53 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

CFLAGS = -Wall -Werror -Wextra -g

SPATH = ./srcs/
SRCS = allocate_memory.c \
	   get_buckets.c \
	   new_bucket.c \
	   utils.c \
	   lib.c \
	   show_alloc_mem.c \
	   malloc.c \
	   calloc.c \
	   realloc.c \
	   free.c

OPATH = ./.objs/
OBJS = $(addprefix $(OPATH),$(SRCS:.c=.o))

INCS = -I./incs/ -I./printf/incs/

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

LDFLAGS = -L./printf/ -lftprintf

NAME = libft_malloc_$(HOSTTYPE).so 

all: $(NAME)

$(NAME): $(OBJS)
	make -j8 -C ./printf/
	$(CC) -shared $(OBJS) -Wl,-install_name,$(NAME) $(LDFLAGS) -o $(NAME)
	ln -fs $(NAME) libft_malloc.so

$(OPATH)%.o: $(SPATH)%.c
	@mkdir -p $(OPATH)
	$(CC) -fPIC $(CFLAGS) $(INCS) -o $@ -c $<

clean:
	make -j8 -C ./printf/ clean
	rm -r $(OPATH)

fclean:
	make -j8 -C ./printf/ fclean
	rm -rf $(OPATH)
	rm -f $(NAME)
	rm -f libft_malloc.so

re: fclean all
