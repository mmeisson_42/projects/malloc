/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 11:40:59 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/17 16:16:43 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <pthread.h>
# include <stdbool.h>
# include <sys/mman.h>
# include <unistd.h>
# include "ft_stdio.h"

enum					e_mem_size
{
	TINY = 256,
	SMALL = 1024,
	BIG,
};

struct					s_meta_mem
{
	size_t		size;
	void		*mem;
};

struct					s_bucket
{
	struct s_bucket		*next;
	size_t				capacity;
	size_t				full_size;
	struct s_meta_mem	chunks[];
};

struct					s_bucketlock
{
	struct s_bucket			*list;
	pthread_mutex_t			lock;
	size_t					bucket_number;
};

/*
**		new_bucket.c
*/
struct s_bucket			*new_bucket(enum e_mem_size size);

/*
**		utils.c
*/
void					*memory_map(size_t size);
bool					bucket_is_empty(struct s_bucket *this);
void					remove_bucket_from(struct s_bucket **from,
		struct s_bucket *this);
void					insert_bucket(struct s_bucket **first,
		struct s_bucket *new);
void					*allocate_memory(struct s_bucketlock *lock,
		size_t size);

/*
**		get_buckets.c
*/
struct s_bucketlock		*get_buckets_by_size(size_t size);
struct s_bucketlock		*get_buckets_by_ind(size_t ind);
struct s_bucketlock		*get_buckets_by_ptr(void *ptr);
struct s_bucket			*get_bucket_in_buckets_by_ptr(struct s_bucketlock *l,
		void *ptr);
struct s_meta_mem		*get_mem_in_ptr(struct s_bucket *b, void *ptr);

enum e_mem_size			size_to_chunck_size(size_t size);

/*
**		free.c
*/
void					free_the_bucket(struct s_bucketlock *l,
		struct s_bucket *b);

/*
**		lib.c
*/
void					*ft_memcpy(void *dst, void *src, size_t size);
size_t					ft_min(size_t v1, size_t v2);

void					*malloc(size_t size);
void					*realloc(void *ptr, size_t size);
void					free(void *ptr);
void					show_alloc_mem(void);

#endif
