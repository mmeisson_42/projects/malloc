/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   allocate_bucket.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 14:28:16 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/17 13:27:30 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void			link_bucket(struct s_bucket *new, enum e_mem_size size)
{
	size_t		i;

	i = 0;
	while (i < new->capacity)
	{
		new->chunks[i].size = 0;
		new->chunks[i].mem = (char *)new + sizeof(struct s_bucket) +
			sizeof(struct s_meta_mem) * 100 +
			i * size;
		i++;
	}
}

struct s_bucket		*new_bucket(enum e_mem_size size)
{
	struct s_bucket		*new;
	size_t				page;
	size_t				mem_size;

	page = (size_t)getpagesize();
	mem_size = (size + sizeof(struct s_meta_mem)) * 100 +
		sizeof(struct s_bucket);
	mem_size = ((mem_size / page) + 1) * page;
	new = memory_map(mem_size);
	if (new)
	{
		new->next = NULL;
		new->full_size = mem_size;
		new->capacity = (mem_size - sizeof(struct s_bucket)) /
			(size + sizeof(struct s_meta_mem));
		link_bucket(new, size);
	}
	return (new);
}
