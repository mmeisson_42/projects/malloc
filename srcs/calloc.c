/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/16 08:11:52 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/17 16:05:05 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void		mzero(void *ptr, size_t size)
{
	size_t			i;
	unsigned char	*word;

	i = 0;
	word = (unsigned char *)ptr;
	while (i < size)
	{
		word[i] = 0;
		i++;
	}
}

void			*calloc(size_t count, size_t size)
{
	struct s_bucketlock		*l;
	void					*mem;

	if (size == 0 || count == 0)
		return (NULL);
	l = get_buckets_by_size(count * size);
	pthread_mutex_lock(&l->lock);
	mem = allocate_memory(l, count * size);
	if (mem)
		mzero(mem, count * size);
	pthread_mutex_unlock(&l->lock);
	return (mem);
}
