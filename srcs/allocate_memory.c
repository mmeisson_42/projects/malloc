/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   allocate_memory.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/11 11:53:24 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/17 13:53:41 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

enum e_mem_size		size_to_chunck_size(size_t size)
{
	if (size <= TINY)
		return (TINY);
	else if (size <= SMALL)
		return (SMALL);
	else
		return (BIG);
}

static void			*big_allocate_memory(struct s_bucketlock *l, size_t size)
{
	struct s_bucket		*new;
	size_t				full_mem;

	full_mem = sizeof(*new) + sizeof(struct s_meta_mem) + size;
	new = memory_map(full_mem);
	if (new == NULL)
		return (NULL);
	new->next = NULL;
	new->capacity = 1;
	new->full_size = full_mem;
	new->chunks[0].size = size;
	new->chunks[0].mem = (char *)new + sizeof(struct s_bucket) +
		sizeof(struct s_meta_mem);
	insert_bucket(&l->list, new);
	l->bucket_number++;
	return (new->chunks[0].mem);
}

static void			*allocate_new_memory(struct s_bucketlock *l, size_t size)
{
	struct s_bucket	*new;

	new = new_bucket(size_to_chunck_size(size));
	if (!new)
		return (NULL);
	insert_bucket(&l->list, new);
	l->bucket_number++;
	new->chunks[0].size = size;
	return (new->chunks[0].mem);
}

void				*allocate_memory(struct s_bucketlock *l, size_t size)
{
	size_t			i;
	struct s_bucket	*iter;

	if (size_to_chunck_size(size) == BIG)
		return (big_allocate_memory(l, size));
	iter = l->list;
	while (iter != NULL)
	{
		i = 0;
		while (i < iter->capacity)
		{
			if (iter->chunks[i].size == 0)
			{
				iter->chunks[i].size = size;
				return (iter->chunks[i].mem);
			}
			i++;
		}
		iter = iter->next;
	}
	return (allocate_new_memory(l, size));
}
