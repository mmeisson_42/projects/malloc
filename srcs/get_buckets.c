/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_buckets.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 11:54:57 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/13 21:11:20 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

struct s_bucketlock		*get_buckets_by_ind(size_t ind)
{
	static struct s_bucketlock		array[3] = {
		{NULL, PTHREAD_MUTEX_INITIALIZER, 0},
		{NULL, PTHREAD_MUTEX_INITIALIZER, 0},
		{NULL, PTHREAD_MUTEX_INITIALIZER, 0},
	};

	if (ind > 2)
		ind = 2;
	return (&array[ind]);
}

struct s_bucketlock		*get_buckets_by_size(size_t size)
{
	if (size < TINY)
	{
		return (get_buckets_by_ind(0));
	}
	else if (size < SMALL)
	{
		return (get_buckets_by_ind(1));
	}
	return (get_buckets_by_ind(2));
}

struct s_meta_mem		*get_mem_in_ptr(struct s_bucket *b, void *ptr)
{
	size_t		i;

	i = 0;
	while (i < b->capacity)
	{
		if (ptr == (char *)(b->chunks[i].mem))
			return (&b->chunks[i]);
		i++;
	}
	return (NULL);
}

struct s_bucket			*get_bucket_in_buckets_by_ptr(struct s_bucketlock *l,
		void *ptr)
{
	struct s_bucket		*iter;

	iter = l->list;
	while (iter)
	{
		if (get_mem_in_ptr(iter, ptr))
			return (iter);
		iter = iter->next;
	}
	return (NULL);
}

struct s_bucketlock		*get_buckets_by_ptr(void *ptr)
{
	struct s_bucketlock		*l;
	size_t					i;

	i = 0;
	while (i < 3)
	{
		l = get_buckets_by_ind(i);
		if (get_bucket_in_buckets_by_ptr(l, ptr))
			return (l);
		i++;
	}
	return (NULL);
}
