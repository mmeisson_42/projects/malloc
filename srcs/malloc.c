/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 15:45:40 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/17 16:11:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		*malloc(size_t size)
{
	struct s_bucketlock		*l;
	void					*mem;

	if (size == 0)
		return (NULL);
	l = get_buckets_by_size(size);
	pthread_mutex_lock(&l->lock);
	mem = allocate_memory(l, size);
	pthread_mutex_unlock(&l->lock);
	return (mem);
}
