/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/10 11:47:36 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/13 18:41:32 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		*memory_map(size_t size)
{
	return (mmap(
				NULL,
				size,
				PROT_READ | PROT_WRITE,
				MAP_ANON | MAP_PRIVATE,
				0,
				0));
}

bool		bucket_is_empty(struct s_bucket *this)
{
	size_t		i;

	i = 0;
	while (i < this->capacity)
	{
		if (this->chunks[i].size != 0)
		{
			return (false);
		}
		i++;
	}
	return (true);
}

void		remove_bucket_from(struct s_bucket **from, struct s_bucket *this)
{
	struct s_bucket		*iter;
	struct s_bucket		*tmp;

	if (*from == NULL)
		return ;
	iter = *from;
	if (this == iter)
	{
		*from = iter->next;
		munmap(iter, iter->full_size);
	}
	else
	{
		while (iter->next)
		{
			if (iter->next == this)
			{
				tmp = iter->next;
				iter->next = iter->next->next;
				munmap(tmp, tmp->full_size);
				break ;
			}
			iter = iter->next;
		}
	}
}

/*
**	insert_bucket make a sorted insertion (by address)
*/

void		insert_bucket(struct s_bucket **first, struct s_bucket *new)
{
	struct s_bucket		*iter;

	if (*first == NULL || *first > new)
	{
		new->next = *first;
		*first = new;
	}
	else
	{
		iter = *first;
		while (iter->next && iter->next < new)
			iter = iter->next;
		new->next = iter->next;
		iter->next = new;
	}
}
