/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/13 14:29:25 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/13 21:36:35 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

size_t		print_bucket(const char *name, struct s_bucket *list)
{
	size_t		i;
	size_t		total;
	size_t		size;

	total = 0;
	ft_printf("%s: %#x\n", name, list);
	while (list)
	{
		i = 0;
		while (i < list->capacity)
		{
			size = list->chunks[i].size;
			total += size;
			if (size)
			{
				ft_printf("%#x - %#x : %u octets\n",
						list->chunks[i].mem,
						list->chunks[i].mem + size,
						size);
			}
			i++;
		}
		list = list->next;
	}
	return (total);
}

void		show_alloc_mem(void)
{
	const char			str[3][8] = {
		"TINY", "SMALL", "LARGE",
	};
	size_t				i;
	size_t				total;
	struct s_bucketlock	*l;

	i = 0;
	total = 0;
	while (i < 3)
	{
		l = get_buckets_by_ind(i);
		if (l)
		{
			pthread_mutex_lock(&l->lock);
			if (l->list)
				total += print_bucket(str[i], l->list);
			pthread_mutex_unlock(&l->lock);
		}
		i++;
	}
	ft_printf("Total : %D\n", total);
}
