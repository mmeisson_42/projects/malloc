/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/11 11:12:57 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/13 17:58:07 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void		free_the_bucket(struct s_bucketlock *l, struct s_bucket *b)
{
	if ((bucket_is_empty(b) && l->bucket_number > 1) || b->capacity == 1)
	{
		l->bucket_number--;
		remove_bucket_from(&l->list, b);
	}
}

void		free(void *ptr)
{
	struct s_bucketlock		*l;
	struct s_bucket			*b;
	struct s_meta_mem		*m;

	l = get_buckets_by_ptr(ptr);
	if (l)
	{
		pthread_mutex_lock(&l->lock);
		b = get_bucket_in_buckets_by_ptr(l, ptr);
		if (b)
		{
			m = get_mem_in_ptr(b, ptr);
			if (m)
			{
				m->size = 0;
				free_the_bucket(l, b);
			}
		}
		pthread_mutex_unlock(&l->lock);
	}
}
