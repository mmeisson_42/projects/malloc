/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/13 11:48:23 by mmeisson          #+#    #+#             */
/*   Updated: 2017/05/17 16:15:19 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void			*resize_chunck(struct s_meta_mem *meta, size_t size)
{
	meta->size = size;
	return (meta->mem);
}

static void			*move_chunck(struct s_bucketlock *l,
		struct s_meta_mem *meta, size_t size)
{
	void		*mem;

	mem = allocate_memory(l, size);
	if (size == 0)
	{
		meta->size = 0;
		return (NULL);
	}
	if (mem)
	{
		ft_memcpy(mem, meta->mem, ft_min(size, meta->size));
		meta->size = 0;
	}
	return (mem);
}

static void			*manage_copy(struct s_bucketlock *l, struct s_meta_mem *m,
		size_t size)
{
	void		*mem;

	if (size != 0 && size_to_chunck_size(size) != BIG &&
			size_to_chunck_size(size) == size_to_chunck_size(m->size))
		mem = resize_chunck(m, size);
	else
		mem = move_chunck(l, m, size);
	return (mem);
}

void				*realloc(void *ptr, size_t size)
{
	struct s_bucketlock		*l;
	struct s_bucket			*b;
	struct s_meta_mem		*m;
	void					*mem;

	mem = NULL;
	if (ptr == NULL)
		return (malloc(size));
	l = get_buckets_by_ptr(ptr);
	if (l == NULL)
		return (NULL);
	pthread_mutex_lock(&l->lock);
	b = get_bucket_in_buckets_by_ptr(l, ptr);
	if (b)
	{
		m = get_mem_in_ptr(b, ptr);
		if (m)
		{
			mem = manage_copy(l, m, size);
		}
	}
	pthread_mutex_unlock(&l->lock);
	return (mem);
}
